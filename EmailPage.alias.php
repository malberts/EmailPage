<?php
/**
 * Aliases for Special:EmailPage
 *
 * @file
 * @ingroup Extensions
 */

$aliases = [];

/** English
 * @author Jon Harald Søby
 */
$aliases['en'] = [
	'EmailPage' => [ 'EmailPage' ],
];

/** Arabic (العربية]
 * @author Meno25
 */
$aliases['ar'] = [
	'EmailPage' => [ 'مراسلة_الصفحة' ],
];

/** Egyptian Spoken Arabic (مصرى]
 * @author Meno25
 */
$aliases['arz'] = [
	'EmailPage' => [ 'مراسلة_الصفحة' ],
];

/** French (Français] */
$aliases['fr'] = [
	'EmailPage' => [ 'EnvoyerPage', 'Envoyer Page' ],
];

/** Galician (Galego] */
$aliases['gl'] = [
	'EmailPage' => [ 'Enviar páxinas por correo electrónico' ],
];

/** Hebrew (עברית]
 * @author Rotem Liss
 */
$aliases['he'] = [
	'EmailPage' => [ 'שליחת_דף_בדואר' ],
];

/** Haitian (Kreyòl ayisyen] */
$aliases['ht'] = [
	'EmailPage' => [ 'EmèlAtik' ],
];

/** Hungarian (Magyar] */
$aliases['hu'] = [
	'EmailPage' => [ 'Szócikk elküldése e-mailben' ],
];

/** Luxembourgish (Lëtzebuergesch] */
$aliases['lb'] = [
	'EmailPage' => [ 'E-Mail-Säiten' ],
];

/** Dutch (Nederlands] */
$aliases['nl'] = [
	'EmailPage' => [ 'PaginaE-mailen', 'PaginaEmailen' ],
];

/** Norwegian (bokmål]‬ (‪Norsk (bokmål]‬]
 * @author Jon Harald Søby
 */
$aliases['no'] = [
	'EmailPage' => [ 'Send side som e-post' ],
];

/** Pashto (پښتو] */
$aliases['ps'] = [
	'EmailPage' => [ 'د برېښليک مخونه' ],
];
